#ifndef AUTO_UPDATE_H
#define AUTO_UPDATE_H

#include <WiFi.h>
#include <HTTPClient.h>
#include "config.h"
#include "SerialBuffer.h"

extern String updateUrl;
extern bool autoUpdateEnabled;
extern unsigned long lastUpdateCheck;
const unsigned long updateInterval = AUTOUPDATE_INTERVAL;

extern Preferences preferences;

void loadUpdateSettings() {
    logMessage("Retrieving saved settings from NVS", LOG_INFO);
    preferences.begin("settings", false);
    updateUrl = preferences.getString("updateUrl", "");
    autoUpdateEnabled = preferences.getBool("autoUpdate", false);
    preferences.end();
}

void checkForUpdates() {
    if (autoUpdateEnabled && WiFi.status() == WL_CONNECTED) {
        logMessage("Checking for updates...", LOG_INFO);
        HTTPClient http;
        http.begin(updateUrl + "/version.txt");
        int httpCode = http.GET();

        if (httpCode == HTTP_CODE_OK) {
            String newVersion = http.getString();
            if (newVersion != CURRENT_VERSION) {
                logMessage("New version available: " + newVersion + ". Starting update...", LOG_INFO);
                http.begin(updateUrl + "/firmware.bin");
                int httpCode = http.GET();

                if (httpCode == HTTP_CODE_OK) {
                    int len = http.getSize();
                    uint8_t buff[128] = { 0 };
                    WiFiClient* stream = http.getStreamPtr();
                    Update.begin(len);
                    
                    while (http.connected() && (len > 0 || len == -1)) {
                        size_t size = stream->available();
                        if (size) {
                            int c = stream->readBytes(buff, ((size > sizeof(buff)) ? sizeof(buff) : size));
                            Update.write(buff, c);
                            if (len > 0) {
                                len -= c;
                            }
                        }
                        delay(1);
                    }
                    
                    if (Update.end(true)) {
                        logMessage("Update successfully completed. Rebooting.", LOG_INFO);
                        ESP.restart();
                    } else {
                        logMessage("Update failed.", LOG_ERROR);
                    }
                } else {
                    logMessage("Failed to download firmware. HTTP code: " + String(httpCode), LOG_ERROR);
                }
            } else {
                logMessage("No new version available.", LOG_INFO);
            }
        } else {
            logMessage("Failed to check version. HTTP code: " + String(httpCode), LOG_ERROR);
        }
        http.end();
    } else {
        logMessage("Auto-update not enabled or WiFi not connected.", LOG_WARNING);
    }
}

#endif // AUTO_UPDATE_H
