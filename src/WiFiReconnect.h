#ifndef WIFI_RECONNECT_H
#define WIFI_RECONNECT_H

#include <WiFi.h>
#include "SerialBuffer.h"

extern unsigned long previousReconnectAttempt;
extern const unsigned long reconnectInterval;
extern bool wasDisconnected;

void reconnectWiFi() {
    if (WiFi.status() != WL_CONNECTED) {
        unsigned long currentMillis = millis();
        if (currentMillis - previousReconnectAttempt >= reconnectInterval) {
            previousReconnectAttempt = currentMillis;
            logMessage("WiFi connection lost. Attempting to reconnect...", LOG_WARNING);
            WiFi.disconnect();
            WiFi.begin(); // Attempt to reconnect
            wasDisconnected = true;
        }
    } else {
        // Only print the reconnection message if we were previously disconnected
        if (wasDisconnected) {
            logMessage("Reconnected to WiFi.", LOG_INFO);
            wasDisconnected = false;
        }
    }
}

#endif // WIFI_RECONNECT_H
