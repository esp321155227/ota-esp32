#ifndef WEBSERVERSETUP_H
#define WEBSERVERSETUP_H

#include <WebServer.h>
#include <WiFi.h>
#include "SerialBuffer.h"
#include "html/debug_html.h"
#include "html/root_html.h"
#include "config.h"
#include <ArduinoJson.h>
#include <Preferences.h> 
#include "AutoUpdate.h"

WebServer server(80);

extern String updateUrl;
extern bool autoUpdateEnabled;
extern Preferences preferences;

// Helper function to check authentication
bool isAuthenticated() {
    return server.authenticate(WEB_SERVER_USERNAME, WEB_SERVER_PASSWORD);
}

// Wrapper function to handle routes with optional authentication
void handleRequest(void (*handler)(), bool requireAuth = false) {
    if (requireAuth && !isAuthenticated()) {
        server.requestAuthentication();
        return;
    }
    handler();
}

// Frontend Handlers (Pages)
// Handle the root URL
void handlePageRoot() {
    server.send_P(200, "text/html", root_html);
}

// Handle the debug URL
void handlePageDebug() {
    server.send_P(200, "text/html", debug_html);
}

// Backend Handlers (API Endpoints)
// Handle 404 errors
void handleNotFound() {
    server.send(404, "text/plain", "404: Not found");
}

// Handle ESP32 reset
void handleApiEspReset() {
    server.send(200, "text/plain", "OK");
    delay(1000);  // Delay to allow the response to be sent
    ESP.restart();
}

// Handle serial buffer display
void handleApiSerial() {
    String message = "";
    for (const auto& line : serialBuffer) {
        message += line + "\n";
    }
    server.send(200, "text/plain", message.length() > 0 ? message : "No serial data available.");
}

// Handle debug info display
void handleApiDebugInfo() {
    StaticJsonDocument<1024> doc;

    doc["signalStrength"] = String(WiFi.RSSI()) + " dBm";
    doc["ipAddress"] = WiFi.localIP().toString();
    doc["uptime"] = String(millis() / 1000) + " seconds";
    doc["freeHeap"] = String(ESP.getFreeHeap()) + " bytes";
    doc["totalHeap"] = String(ESP.getHeapSize()) + " bytes";
    doc["freePsram"] = String(ESP.getFreePsram()) + " bytes";
    doc["totalPsram"] = String(ESP.getPsramSize()) + " bytes";
    doc["sketchSize"] = String(ESP.getSketchSize()) + " bytes";
    doc["freeSketchSpace"] = String(ESP.getFreeSketchSpace()) + " bytes";
    doc["chipRevision"] = String(ESP.getChipRevision());
    doc["sdkVersion"] = String(ESP.getSdkVersion());
    doc["cpuFreqMHz"] = String(ESP.getCpuFreqMHz()) + " MHz";
    doc["flashChipSize"] = String(ESP.getFlashChipSize()) + " bytes";
    doc["flashChipSpeed"] = String(ESP.getFlashChipSpeed() / 1000000) + " MHz";
    doc["macAddress"] = WiFi.macAddress();

    String json;
    serializeJson(doc, json);
    server.send(200, "application/json", json);
}

// Handle log download
void handleApiLogDownload() {
    String logContent = "";
    for (const auto& line : serialBuffer) {
        logContent += line + "\n";
    }
    server.sendHeader("Content-Type", "text/plain");
    server.sendHeader("Content-Disposition", "attachment; filename=log.txt");
    server.send(200, "text/plain", logContent);
}

// Functions for handling automatic update settings
void handleApiUpdateSettings() {
    if (server.hasArg("plain")) {
        StaticJsonDocument<1024> doc;
        deserializeJson(doc, server.arg("plain"));
        updateUrl = doc["url"].as<String>();
        autoUpdateEnabled = doc["autoUpdate"];

        // Saving settings to NVS
        preferences.begin("settings", false);
        preferences.putString("updateUrl", updateUrl);
        preferences.putBool("autoUpdate", autoUpdateEnabled);
        preferences.end();

        server.send(200, "application/json", "{\"success\":true}");
    } else {
        server.send(400, "application/json", "{\"success\":false}");
    }
}

// Function to handle getting the current update settings
void handleApiGetUpdateSettings() {
    StaticJsonDocument<1024> doc;
    doc["url"] = updateUrl;
    doc["autoUpdate"] = autoUpdateEnabled;

    String response;
    serializeJson(doc, response);
    server.send(200, "application/json", response);
}

// -------------------------------------------------------
// Template for new endpoint handler function
/*
void handleNewEndpoint() {
    // Implementation of new endpoint
    server.send(200, "text/plain", "This is the new endpoint response");
}
*/
// -------------------------------------------------------

// Function to setup Web server
void setupWebServer() {
    // Root endpoint with authentication
    server.on("/", HTTP_GET, []() {
        handleRequest(handlePageRoot);
    });

    // Debug page endpoint with authentication
    server.on("/debug", HTTP_GET, []() {
        handleRequest(handlePageDebug, true);
    });

    // Serial endpoint with authentication
    server.on("/api/serial", HTTP_GET, []() {
        handleRequest(handleApiSerial, true);
    });

    // Reset endpoint with authentication
    server.on("/api/rst", HTTP_GET, []() {
        handleRequest(handleApiEspReset, true);
    });

    // Debug info endpoint with authentication
    server.on("/api/debug-info", HTTP_GET, []() {
        handleRequest(handleApiDebugInfo, true);
    });

    // Log download endpoint with authentication
    server.on("/api/download-log", HTTP_GET, []() {
        handleRequest(handleApiLogDownload, true);
    });

    // Endpoint to handle automatic update settings
    server.on("/api/update-settings", HTTP_POST, []() {
        handleRequest(handleApiUpdateSettings, true);
    });

    // Endpoint to get current update settings
    server.on("/api/get-update-settings", HTTP_GET, []() {
        handleRequest(handleApiGetUpdateSettings, true);
    });

    // -------------------------------------------------------
    // Template for new endpoints
    // Add your new endpoints here using the same pattern
    // Example:
    // server.on("/new-endpoint", HTTP_GET, []() {
    //     handleRequest(handleNewEndpoint, true); // Set to true if authentication is required
    // });
    //--------------------------------------------------------

    server.onNotFound(handleNotFound); // 404 handler
    server.begin();
}

#endif
