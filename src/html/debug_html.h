#ifndef DEBUG_HTML_H
#define DEBUG_HTML_H

const char debug_html[] PROGMEM = R"rawliteral(
<!DOCTYPE html>
<html>
<head>
<title>ESP32 Web Server</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<style>
body { font-family: Arial, sans-serif; background-color: #f4f4f4; margin: 0; padding: 0; }
header { background-color: #333; color: #fff; padding: 10px 0; text-align: center; }
main { padding: 20px; padding-bottom: 60px; /* Add padding to avoid footer overlap */ }
button { padding: 10px 20px; margin: 10px 0; font-size: 16px; }
table { width: 100%; border-collapse: collapse; margin-top: 20px; }
table, th, td { border: 1px solid #ddd; }
th, td { padding: 8px; text-align: left; }
th { background-color: #f2f2f2; }
.info { background-color: #d9edf7; color: #31708f; }
.warning { background-color: #fcf8e3; color: #8a6d3b; }
.error { background-color: #f2dede; color: #a94442; }
#tableContainer { max-height: 300px; overflow-y: auto; } /* Scrollable table container */
footer { background-color: #333; color: #fff; text-align: center; padding: 10px 0; position: relative; bottom: 0; width: 100%; /* Change to relative position */ }
</style>
<script>
function sendResetRequest() {
    fetch('api/rst').then(response => {
        if (response.ok) {
            alert('ESP32 is resetting...');
        } else {
            alert('Reset failed.');
        }
    });
}

function fetchSerialData() {
    fetch('/api/serial')
        .then(response => response.text())
        .then(data => {
            const rows = data.split('-;-').filter(line => line.trim() !== '').map(line => {
                let logClass = 'info';
                if (line.includes('[ERROR]')) {
                    logClass = 'error';
                } else if (line.includes('[WARNING]')) {
                    logClass = 'warning';
                }
                return `<tr class="${logClass}"><td>${line}</td></tr>`;
            }).join('');
            document.getElementById('serialData').innerHTML = rows;
            scrollToBottom(); // Scroll to the latest message
        });
}

function fetchDebugInfo() {
    fetch('api/debug-info')
        .then(response => response.json())
        .then(data => {
            document.getElementById('signalStrength').innerText = data.signalStrength;
            document.getElementById('ipAddress').innerText = data.ipAddress;
            document.getElementById('uptime').innerText = data.uptime;
            document.getElementById('freeHeap').innerText = data.freeHeap;
            document.getElementById('totalHeap').innerText = data.totalHeap;
            document.getElementById('freePsram').innerText = data.freePsram;
            document.getElementById('totalPsram').innerText = data.totalPsram;
            document.getElementById('sketchSize').innerText = data.sketchSize;
            document.getElementById('freeSketchSpace').innerText = data.freeSketchSpace;
            document.getElementById('chipRevision').innerText = data.chipRevision;
            document.getElementById('sdkVersion').innerText = data.sdkVersion;
            document.getElementById('cpuFreqMHz').innerText = data.cpuFreqMHz;
            document.getElementById('flashChipSize').innerText = data.flashChipSize;
            document.getElementById('flashChipSpeed').innerText = data.flashChipSpeed;
            document.getElementById('macAddress').innerText = data.macAddress;
        });
}

function fetchUpdateSettings() {
    fetch('/api/get-update-settings')
        .then(response => response.json())
        .then(data => {
            document.getElementById('updateUrl').value = data.url;
            document.getElementById('autoUpdate').checked = data.autoUpdate;
        });
}

function downloadLog() {
    window.location.href = 'api/download-log';
}

function saveUpdateSettings() {
    const url = document.getElementById('updateUrl').value;
    const autoUpdate = document.getElementById('autoUpdate').checked;

    fetch('/api/update-settings', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({ url, autoUpdate })
    })
    .then(response => response.json())
    .then(data => {
        if (data.success) {
            alert('Settings saved successfully.');
        } else {
            alert('Failed to save settings.');
        }
    });
}

function scrollToBottom() {
    var tableContainer = document.getElementById('tableContainer');
    tableContainer.scrollTop = tableContainer.scrollHeight;
}

window.onload = function() {
    fetchSerialData();
    fetchDebugInfo();
    fetchUpdateSettings();
    setInterval(fetchSerialData, 5000); // Fetch data every 5 seconds
    setInterval(fetchDebugInfo, 10000); // Fetch debug info every 10 seconds
}
</script>
</head>
<body>
<header>
<h1>Welcome to ESP32 Web Server</h1>
</header>
<main>
<button onclick="sendResetRequest()">Reset ESP32</button>
<button onclick="downloadLog()">Download Log</button>

<h2>Serial Data</h2>
<div id="tableContainer">
<table>
<thead>
<tr><th>Data</th></tr>
</thead>
<tbody id="serialData">
<!-- Serial data will be inserted here -->
</tbody>
</table>
</div>

<h2>Auto Update Settings</h2>
<p><strong>Update URL:</strong> <input type="text" id="updateUrl" placeholder="http://example.com/update/"></p>
<p><input type="checkbox" id="autoUpdate"> Enable Auto Update</p>
<button onclick="saveUpdateSettings()">Save Settings</button>

<h2>Debug Information</h2>
<p><strong>Signal Strength:</strong> <span id="signalStrength">N/A</span></p>
<p><strong>IP Address:</strong> <span id="ipAddress">N/A</span></p>
<p><strong>Uptime:</strong> <span id="uptime">N/A</span></p>
<p><strong>Free Heap:</strong> <span id="freeHeap">N/A</span></p>
<p><strong>Total Heap:</strong> <span id="totalHeap">N/A</span></p>
<p><strong>Free PSRAM:</strong> <span id="freePsram">N/A</span></p>
<p><strong>Total PSRAM:</strong> <span id="totalPsram">N/A</span></p>
<p><strong>Sketch Size:</strong> <span id="sketchSize">N/A</span></p>
<p><strong>Free Sketch Space:</strong> <span id="freeSketchSpace">N/A</span></p>
<p><strong>Chip Revision:</strong> <span id="chipRevision">N/A</span></p>
<p><strong>SDK Version:</strong> <span id="sdkVersion">N/A</span></p>
<p><strong>CPU Frequency:</strong> <span id="cpuFreqMHz">N/A</span></p>
<p><strong>Flash Chip Size:</strong> <span id="flashChipSize">N/A</span></p>
<p><strong>Flash Chip Speed:</strong> <span id="flashChipSpeed">N/A</span></p>
<p><strong>MAC Address:</strong> <span id="macAddress">N/A</span></p>

</main>
<footer>
<p>&copy; 2024 Dominik Campa - OTA-ESP32</p>
</footer>
</body>
</html>
)rawliteral";

#endif // DEBUG_HTML_H
