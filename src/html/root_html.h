#ifndef ROOT_HTML_H
#define ROOT_HTML_H

const char root_html[] PROGMEM = R"rawliteral(
<!DOCTYPE html>
<html>
<head>
<title>ESP32 Web Server</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<style>
body { font-family: Arial, sans-serif; background-color: #f4f4f4; margin: 0; padding: 0; }
header { background-color: #333; color: #fff; padding: 10px 0; text-align: center; }
main { padding: 20px; padding-bottom: 60px; /* Add padding to avoid footer overlap */ }
button { padding: 10px 20px; margin: 10px 0; font-size: 16px; }
footer { background-color: #333; color: #fff; text-align: center; padding: 10px 0; position: relative; bottom: 0; width: 100%; /* Change to relative position */ }
</style>
</head>
<body>
<header>
<h1>Welcome to ESP32 Web Server</h1>
</header>
<main>
<h2>Project Information</h2>
<p>This project is a template for setting up an ESP32 web server with OTA updates, mDNS, NTP client, and WiFi management. The template includes various endpoints for debugging and monitoring the ESP32 device.</p>

<h3>Features:</h3>
<ul>
    <li>Web server with authentication</li>
    <li>Over-the-Air (OTA) updates</li>
    <li>mDNS for easy access</li>
    <li>NTP client for time synchronization</li>
    <li>WiFi manager for network configuration</li>
    <li>Serial log viewing and downloading</li>
    <li>ESP32 reset functionality</li>
</ul>

<h3>How to Use:</h3>
<ol>
    <li>Connect to the ESP32's WiFi network or configure it to connect to your local WiFi network using the captive portal.</li>
    <li>Access the web interface by navigating to the ESP32's IP address or using the mDNS name (<a href="http://esp32.local">http://esp32.local</a>).</li>
    <li>Log in using the credentials specified in the <code>config.h</code> file (default username: <strong>admin</strong>, default password: <strong>password</strong>).</li>
    <li>Use the web interface to view serial logs, reset the ESP32, and download logs.</li>
    <li>To update the firmware, use the OTA functionality available through the web interface.</li>
</ol>

<h3>OTA Configuration:</h3>
<pre>
[env:esp32dev]
platform = espressif32
board = esp32dev
framework = arduino
upload_protocol = espota
upload_port = 192.168.1.51  ; IP address of your ESP32 device
upload_flags =
    --auth=1234 ; Password for OTA
    --timeout=60
lib_deps =
    WiFi
    WiFiClientSecure
    Arduino_DebugUtils
    Arduino_ESP32_OTA
    ArduinoHttpClient
    arduino-libraries/NTPClient @ ^3.1.0
    https://github.com/tzapu/WiFiManager.git
</pre>

<h3>Adding New Endpoints:</h3>
<p>To add a new endpoint, follow these steps:</p>
<ol>
    <li>Create a new handler function in the <code>WebServerSetup.h</code> file. For example:
    <pre>
void handleNewEndpoint() {
    server.send(200, "text/plain", "This is the new endpoint response");
}
    </pre>
    </li>
    <li>Register the new endpoint in the <code>setupWebServer</code> function. For example:
    <pre>
server.on("/new-endpoint", HTTP_GET, []() {
    handleRequest(handleNewEndpoint, true); // Set to true if authentication is required
});
    </pre>
    </li>
</ol>
<p>After adding and testing your new endpoints, you can remove or comment out the root page handler if it is no longer needed.</p>

<h3>Endpoints:</h3>
<ul>
    <li><strong>Root Page:</strong> Provides information about the project.</li>
    <li><strong>/debug:</strong> Debug page with serial log and device info.</li>
    <li><strong>/api/serial:</strong> Endpoint to view serial logs (requires authentication).</li>
    <li><strong>/api/rst:</strong> Endpoint to reset the ESP32 (requires authentication).</li>
    <li><strong>/api/debug-info:</strong> Endpoint to get debug information in JSON format (requires authentication).</li>
    <li><strong>/api/download-log:</strong> Endpoint to download the serial log as a text file (requires authentication).</li>
</ul>

<button onclick="window.location.href='/debug'">Go to Debug Page</button>

</main>
<footer>
<p>&copy; 2024 Dominik Campa - OTA-ESP32</p>
</footer>
</body>
</html>
)rawliteral";

#endif // ROOT_HTML_H
