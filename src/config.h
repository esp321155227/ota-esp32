#ifndef CONFIG_H
#define CONFIG_H

// Web Server Credentials
#define WEB_SERVER_USERNAME "admin"     // Username for web server
#define WEB_SERVER_PASSWORD "password"  // Password for web server

// HOSTNAME - MDNSSetup.h
#define HOSTNAME "esp32"         // Hostname for mDNS and OTA

// WiFi - WiFiManagerSetup.h
#define WIFI_AP_NAME "ESP32_AP"         // AP name for WiFiManager
#define WIFI_MANAGER_TIMEOUT 120        // Timeout for captive portal (120 seconds)

// OTA - OTASetup.h
#define OTA_PASSWORD "1234" // Password for OTA

// NTP - NTPClientSetup.h
#define NTP_SERVER "pool.ntp.org"       // NTP server
#define NTP_TIMEZONE 7200               // Timezone offset in seconds
#define NTP_UPDATE_INTERVAL 60000       // Update interval in milliseconds

// Log - SerialBuffer.h
#define LOG_BUFFER_SIZE 50              // Log buffer size - maximum number of lines in the serial terminal

// Auto update interval check - AutoUpdate.h
#define AUTOUPDATE_INTERVAL 3600000 // Check for updates every 1 hour (3600000 ms)

// Version
#define CURRENT_VERSION "08.08.24"

#endif
