#ifndef OTASETUP_H
#define OTASETUP_H

#include <ArduinoOTA.h>
#include "WebServerSetup.h"
#include "config.h"

// Function to setup OTA
void setupOTA() {
    ArduinoOTA.setHostname(HOSTNAME);

    // Optionally set OTA password
    ArduinoOTA.setPassword(OTA_PASSWORD);

    ArduinoOTA.onStart([]() {
        String type;
        if (ArduinoOTA.getCommand() == U_FLASH) {
            type = "sketch";
        } else { // U_SPIFFS
            type = "filesystem";
        }
        Serial.println("Start updating " + type);
        server.stop(); // Stop server during OTA
    });

    ArduinoOTA.onEnd([]() {
        Serial.println("\nEnd");
        server.begin(); // Restart server after OTA
    });

    ArduinoOTA.onError([](ota_error_t error) {
        Serial.printf("Error[%u]: ", error);
        if (error == OTA_AUTH_ERROR) {
            logMessage("OTA Auth Failed", LOG_WARNING);
        } else if (error == OTA_BEGIN_ERROR) {
            logMessage("OTA Begin Failed", LOG_ERROR);
        } else if (error == OTA_CONNECT_ERROR) {
            logMessage("OTA Connect Failed", LOG_ERROR);
        } else if (error == OTA_RECEIVE_ERROR) {
            logMessage("OTA Receive Failed", LOG_ERROR);
        } else if (error == OTA_END_ERROR) {
            logMessage("OTA End Failed", LOG_ERROR);
        }
    });

    ArduinoOTA.begin();
    logMessage("OTA service started", LOG_INFO);
}

#endif
