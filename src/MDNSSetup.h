#ifndef MDNSSETUP_H
#define MDNSSETUP_H

#include <ESPmDNS.h>
#include "config.h"
#include "SerialBuffer.h"

// Function to setup mDNS
void setupMDNS() {
    if (!MDNS.begin(HOSTNAME)) {
        logMessage("Error setting up MDNS responder!", LOG_ERROR);
    } else {
        logMessage("MDNS responder started", LOG_INFO);
        MDNS.addService("http", "tcp", 80); // Add HTTP service for mDNS
    }
}

#endif
