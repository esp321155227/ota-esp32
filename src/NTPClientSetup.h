#ifndef NTPCLIENTSETUP_H
#define NTPCLIENTSETUP_H

#include <NTPClient.h>
#include <WiFiUdp.h>
#include "config.h"

WiFiUDP ntpUDP;
NTPClient timeClient(ntpUDP, NTP_SERVER, NTP_TIMEZONE, NTP_UPDATE_INTERVAL); // NTP client setup

// Function to setup NTP client
void setupNTPClient() {
    timeClient.begin();
}

#endif
