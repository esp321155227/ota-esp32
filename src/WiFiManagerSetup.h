#ifndef WIFIMANAGERSETUP_H
#define WIFIMANAGERSETUP_H

#include <WiFiManager.h>
#include "config.h"

void setupWiFi() {
    WiFiManager wifiManager;

    // Set timeout for the captive portal
    wifiManager.setConfigPortalTimeout(WIFI_MANAGER_TIMEOUT);

    // Automatically connect using WiFiManager
    if (!wifiManager.autoConnect(WIFI_AP_NAME)) {
        Serial.println("Failed to connect and hit timeout");
        ESP.restart();
    }

    Serial.println("Connected to WiFi");
    Serial.print("IP address: ");
    Serial.println(WiFi.localIP());

    Serial.print("web http://");
    Serial.println(WiFi.localIP());

    Serial.print("web http://");
    Serial.print(HOSTNAME);
    Serial.println(".local");
}

#endif
