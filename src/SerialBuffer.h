#ifndef SERIALBUFFER_H
#define SERIALBUFFER_H

#include <deque>
#include <Arduino.h>
#include "NTPClientSetup.h"  // Include the NTPClient setup to get the current time

// Enum for log levels
enum LogLevel {
    LOG_INFO,
    LOG_WARNING,
    LOG_ERROR
};

std::deque<String> serialBuffer;  // Deque to store the last 50 lines

// Function to convert log level to string
String logLevelToString(LogLevel level) {
    switch (level) {
        case LOG_INFO: return "INFO";
        case LOG_WARNING: return "WARNING";
        case LOG_ERROR: return "ERROR";
        default: return "UNKNOWN";
    }
}

// Function to add messages to the serial buffer with a timestamp and log level
void logMessage(const String& message, LogLevel level = LOG_INFO) {
    // Get the current time as a formatted string
    String formattedTime = timeClient.getFormattedTime();

    // Create the full message with the timestamp and log level
    String fullMessage = formattedTime + " [" + logLevelToString(level) + "] - " + message + "-;-";

    // Add the message to the buffer
    serialBuffer.push_back(fullMessage);

    // Print the message to the serial monitor
    Serial.println(fullMessage);

    // Maintain only the last 50 lines
    if (serialBuffer.size() > LOG_BUFFER_SIZE) {
        serialBuffer.pop_front();
    }
}

#endif
