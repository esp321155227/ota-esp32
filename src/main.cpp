#include "WiFiManagerSetup.h"
#include "OTASetup.h"
#include "WebServerSetup.h"
#include "NTPClientSetup.h"
#include "SerialBuffer.h"
#include "MDNSSetup.h"
#include "config.h"
#include "AutoUpdate.h"
#include "WiFiReconnect.h"

// Definition of external variables
Preferences preferences;
String updateUrl;
bool autoUpdateEnabled = false;
unsigned long lastUpdateCheck = 0;

unsigned long previousReconnectAttempt = 0;
const unsigned long reconnectInterval = 10000; // 10 seconds
bool wasDisconnected = false;

void setup() {
    Serial.begin(9600);
    logMessage("Booting", LOG_INFO);

    loadUpdateSettings(); // Load update settings from NVS

    setupWiFi();
    setupMDNS();
    setupNTPClient();
    setupOTA();
    setupWebServer();
    logMessage("Booted up!", LOG_INFO);
}

void loop() {
    ArduinoOTA.handle();
    server.handleClient();
    timeClient.update();

    // Reconnect to Wi-Fi if disconnected
    reconnectWiFi();

    // Check for automatic update
    unsigned long currentMillis = millis();
    if (currentMillis - lastUpdateCheck >= updateInterval) {
        lastUpdateCheck = currentMillis;
        checkForUpdates();
    }

    delay(1000);
}
