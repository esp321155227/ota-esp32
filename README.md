
# ESP32 Web Server Template

This project serves as a template for setting up an ESP32 web server with various functionalities such as OTA updates, mDNS, NTP client, and WiFi management. It includes multiple endpoints for debugging and monitoring the ESP32 device.

## Features

- Web server with authentication
- Over-the-Air (OTA) updates from a local network
- Automatic updates from a specified URL
- mDNS for easy access
- NTP client for time synchronization
- WiFi manager for network configuration
- Serial log viewing and downloading
- ESP32 reset functionality

## Purpose

This template acts as a "launchpad" for new ESP32 projects. By providing a robust foundation with essential functionalities already implemented, it saves developers significant time. You can use this template as a starting point and simply add your specific features and functionalities, allowing you to focus on the unique aspects of your project rather than rewriting basic firmware.

## How to Use

1. **Connect to the ESP32's WiFi network:**
   - Configure it to connect to your local WiFi network using the captive portal.

2. **Access the web interface:**
   - Navigate to the ESP32's IP address or use the mDNS name: [http://esp32.local](http://esp32.local)

3. **Log in:**
   - Use the credentials specified in the `config.h` file.
   - Default username: **admin**
   - Default password: **password**

4. **Web Interface Features:**
   - View serial logs
   - Reset the ESP32
   - Download logs
   - Configure and enable automatic updates

## OTA Configuration

### Local Network OTA Updates

To configure OTA updates over a local network, add the following to your `platformio.ini` file:

```ini
[env:esp32dev]
platform = espressif32
board = esp32dev
framework = arduino
upload_protocol = espota
upload_port = 192.168.1.51  ; IP address of your ESP32 device
upload_flags = 
	--auth=1234
	--timeout=60
lib_deps = 
	WiFi
	WiFiClientSecure
	Arduino_DebugUtils
	Arduino_ESP32_OTA
	ArduinoHttpClient
	arduino-libraries/NTPClient @ ^3.1.0
	https://github.com/tzapu/WiFiManager.git
	bblanchon/ArduinoJson@^7.1.0
```

### Automatic Update Configuration

The ESP32 can be configured to automatically update its firmware from a specified URL. The update server should host the `firmware.bin` and `version.txt` files.

#### Setting Up the Update Server

1. **Prepare the server files:**
   - Place `firmware.bin` (the compiled firmware binary) and `version.txt` (a text file containing the firmware version) in the server directory.
   
2. **Start the server:**
   - Create a script `start.sh` to start the server:
     ```sh
     php -S 0.0.0.0:80
     ```
   - Run the script to start the server:
     ```sh
     ./start.sh
     ```

3. **Server directory structure:**
   ```
   AutoUpdateServer/
   ├── firmware.bin
   ├── start.sh
   └── version.txt
   ```

4. **Example `version.txt` content:**
   ```
   08.08.24
   ```

#### Version Definition

In the `config.h` file, the version of the firmware is defined, which is used for comparison during the update process:

```cpp
#define CURRENT_VERSION "08.08.24"
```

## Adding New Endpoints

To add a new endpoint:

1. **Create a new handler function:**
    Add your handler function in `WebServerSetup.h`. For example:
    ```cpp
    void handleNewEndpoint() {
        server.send(200, "text/plain", "This is the new endpoint response");
    }
    ```

2. **Register the new endpoint:**
    Register your endpoint in the `setupWebServer` function. For example:
    ```cpp
    server.on("/new-endpoint", HTTP_GET, []() {
        handleRequest(handleNewEndpoint, true); // Set to true if authentication is required
    });
    ```

## Removing the Root Page

After adding and testing your new endpoints, you can remove or comment out the root page handler if it is no longer needed.

## Endpoints

- **Root Page:** Provides information about the project.
- **/debug:** Debug page with serial log and device info (requires authentication).
- **/api/serial:** Endpoint to view serial logs (requires authentication).
- **/api/rst:** Endpoint to reset the ESP32 (requires authentication).
- **/api/debug-info:** Endpoint to get debug information in JSON format (requires authentication).
- **/api/download-log:** Endpoint to download the serial log as a text file (requires authentication).
- **/api/update-settings:** Endpoint to configure automatic update settings (requires authentication).
- **/api/get-update-settings:** Endpoint to retrieve current automatic update settings (requires authentication).